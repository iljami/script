<?php

require_once __DIR__ . '/vendor/autoload.php';

use Entities\Helper\FileMoneyRecordHelper;
use Entities\Taxes\PrintTaxes;

$helper = new FileMoneyRecordHelper();
$printTaxes = new PrintTaxes();

if (empty($argv[1])) {
    die("Please specify a file for processing\n");
} else {
    $file = $argv[1];

    $file_contents = $helper->csvFileToArray($file);

    $printTaxes->printFromArray($file_contents);
}