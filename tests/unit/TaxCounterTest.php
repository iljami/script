<?php

class TaxCounterTest extends \PHPUnit_Framework_TestCase {

    public function testIfSetAndGetCurrencyRatesReturnsTheValueSet()
    {
        $taxCounter = new Entities\Counter\TaxCounter();

        $currency_rates = array(
            "EUR" => 1,
            "USD" => 1.232,
            "UKP" => 0.7,
            "JPY" => 125.55,
        );

        $taxCounter->setCurrencyRates($currency_rates);

        $this->assertEquals($taxCounter->getCurrencyRates(), $currency_rates);
    }

    public function testIfCashInTaxReturnsACorrectTax()
    {
        $taxCounter = new Entities\Counter\TaxCounter();

        $correct_tax = 0.6;
        $this->assertEquals($taxCounter->cashInTax(2000, "EUR"), $correct_tax);
    }

    public function testIfCashOutLegalReturnsACorrectTax()
    {
        $taxCounter = new Entities\Counter\TaxCounter();

        $correct_tax = 0.5;
        $this->assertEquals($taxCounter->cashOutTaxLegal(150, "EUR"), $correct_tax);
    }

    public function testIfCashOutNaturalReturnsACorrectTax()
    {
        $taxCounter = new Entities\Counter\TaxCounter();

        $correct_tax = 1.725;

        $this->assertEquals($taxCounter->cashOutTaxNatural(1575, "EUR", 2, 1000), $correct_tax);
    }
}