<?php

class NaturalCashOutTest extends \PHPUnit_Framework_TestCase {

    public function testIfUpdateCashOutHistoryUpdates()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $data = array(
            0 => '2017-01-01',
            1 => 1222,
            2 => "JPY"
        );

        $cashout->updateCashOutHistory($data[0], $data[1], $data[2]);

        $output = $cashout->getCashOutHistory();

        $this->assertEquals($output[0], $data);
    }

    public function testIfGetLastCashOutRecordReturnsTheLastRecord()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $record_1 = array(
            0 => '2017-02-01',
            1 => 1222,
            2 => "JPY"
        );

        $record_2 = array(
            0 => '2017-07-02',
            1 => 788,
            2 => "EUR"
        );
        $record_3 = array(
            0 => '2015-01-01',
            1 => 15,
            2 => "USD"
        );

        $cashout->updateCashOutHistory($record_1[0], $record_1[1], $record_1[2]);
        $cashout->updateCashOutHistory($record_2[0], $record_2[1], $record_2[2]);
        $cashout->updateCashOutHistory($record_3[0], $record_3[1], $record_3[2]);

        $this->assertEquals($cashout->getLastCashOutRecord(), $record_3);

    }

    public function testIfSetLimitLeftSetsLimit()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $cashout->setLimitLeft(1500);
        $this->assertEquals($cashout->getLimitLeft(), 1500);

    }

    public function testIfCalculateLimitLeftCalulatesTheCorrectLimit()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $this->assertEquals($cashout->calculateLimitLeft(17000, 525, 1), 0);
    }

    public function testIfSetAndUpdateTimesCashTakenSetAndIncrementValue()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $cashout->setTimesCashTaken(2);
        $cashout->updateTimesCashTaken();

        $this->assertEquals($cashout->getTimesCashTaken(), 3);
    }

    public function testWeekNumberFromDateReturnsCorrectValue()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $this->assertEquals($cashout->weekNumberFromDate("2017-12-07"), 49);
    }

    public function testYearFromDateReturnsCorrectValue()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $this->assertEquals($cashout->yearFromDate("2007-12-07"), 2007);
    }

    public function testIfUpdateCurrentWeekUpdatesCurrentWeek()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $data = array(
            0 => '2017-01-01',
            1 => 1222,
            2 => "JPY"
        );

        $cashout->updateCashOutHistory($data[0], $data[1], $data[2]);
        $cashout->updateCurrentWeek();

        $this->assertEquals($cashout->getCurrentWeek(), 52);
    }

    public function testIfUpdateCurrentYearUpdatesCurrentYear()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $data = array(
            0 => '1998-11-13',
            1 => 1885,
            2 => "USD"
        );

        $cashout->updateCashOutHistory($data[0], $data[1], $data[2]);
        $cashout->updateCurrentYear();

        $this->assertEquals($cashout->getCurrentYear(), 1998);
    }

    public function testIfUpdateUpdates()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $data = array(
            0 => "2012-03-29",
            1 => 10000,
            2 => "EUR"
        );

        $cashout->updateCashOutHistory("2011-11-11", 200, "EUR");
        $cashout->updateCurrentWeek();
        $cashout->updateCurrentYear();
        $cashout->setTimesCashTaken(1);
        $cashout->setLimitLeft(0);

        $cashout->update("2012-03-29", 10000, "EUR", 1, $cashout->getLimitLeft());

        $this->assertEquals($cashout->getLastCashOutRecord(), $data);
        $this->assertEquals($cashout->getTimesCashTaken(), 1);
        $this->assertEquals($cashout->getLimitLeft(), 0);
    }

    public function testTimesAndLimitCalculation()
    {
        $cashout = new Entities\CashOut\NaturalCashOut();

        $cashout->updateCashOutHistory("2011-11-11", 1888, "EUR");
        $cashout->updateCurrentWeek();
        $cashout->updateCurrentYear();
        $cashout->setTimesCashTaken(1);
        $cashout->setLimitLeft(0);

        $output = $cashout->calculateTimesAndLimit("2012-07-25");
        $this->assertEquals($output['limit_left'], 1000);
        $this->assertEquals($output['times_cash_taken'], 0);
    }

}