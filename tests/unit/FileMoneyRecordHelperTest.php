<?php

class FileMoneyRecordHelperTest extends \PHPUnit_Framework_TestCase {

    public function testIfCsvFileToArrayReturnsTheFileContentArray()
    {
        $helper = new Entities\Helper\FileMoneyRecordHelper();

        $file = 'test.csv';

        $expected_array = array(
            0 => array(0 => "2015-01-01", 1 => "1", 2 => "natural", 3 => "cash_out", 4 => "1200.00", 5 => "EUR"),
            1 => array(0 => "2015-12-31", 1 => "1", 2 => "natural", 3 => "cash_out", 4 => "1000.00", 5 => "EUR")
        );

        $this->assertEquals($helper->csvFileToArray($file), $expected_array);
    }

    public function testIfCashOutRecordExistsReturnsTrueIfItExists()
    {
        $helper = new Entities\Helper\FileMoneyRecordHelper();

        $person_id = 2;
        $person_array = array(
            $person_id => "Some Information"
        );

        $this->assertTrue($helper->cashOutRecordExists($person_id, $person_array));

    }

    public function testIfCashOutRecordExistsReturnsFalseIfItDoesNotExist()
    {
        $helper = new Entities\Helper\FileMoneyRecordHelper();

        $person_id = 1;
        $person_array = array(
            2 => "Some Information",
            3 => "Some Information"
        );

        $this->assertFalse($helper->cashOutRecordExists($person_id, $person_array));

    }

    public function testIfDotReturnsANumberWithAPointAndTwoDigitsAfterThePoint()
    {
        $helper = new Entities\Helper\FileMoneyRecordHelper();

        $this->assertEquals($helper->dot(25), 25.00);
    }

    public function testRoundUpReturnsCorrectRoundUpSumBasedOnACurrencyWithCents()
    {
        $helper = new Entities\Helper\FileMoneyRecordHelper();

        $this->assertEquals($helper->roundUp(100.223, "EUR"), 100.23);
    }

    public function testRoundUpReturnsCorrectRoundUpSumBasedOnACurrencyWithoutCents()
    {
        $helper = new Entities\Helper\FileMoneyRecordHelper();

        $this->assertEquals($helper->roundUp(100.477, "JPY"), 101);
    }
}