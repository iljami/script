<?php

namespace Entities\Helper;

class FileMoneyRecordHelper
{
    public function csvFileToArray($path_to_file)
    {
        $file_contents = array();
        $i = 0;

        $data = fopen('./' . $path_to_file, "r") or die("Error while loading a file");
        while (!feof($data)) {
            // every line + remove whitespaces from the end of the line
            $line = rtrim(fgets($data));

            $file_contents[$i] = explode(',', $line);
            $i++;
        }

        fclose($data);

        return $file_contents;
    }

    public function cashOutRecordExists($person_id, $history_array)
    {
        $result = false;

        if (empty($history_array)) {
            return false;
        } else {
            foreach ($history_array as $key => $value) {
                if ($key == $person_id) {
                    $result = true;
                    break;
                }
            }
            return $result;
        }
    }

    public function dot($cash)
    {
        try {
            if (!isset($cash) || !is_numeric($cash)){
                throw new \Exception("\n Invalid input for dot function \n");
            } else {
                return number_format($cash, 2, '.', '');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function roundUp($number, $currency, $significance = 0.01)
    {
        try {
            if (empty($currency) || !is_numeric($significance)){
                throw new \Exception("\n Invalid input for RoundUp function. \n");
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        $output = false;
        if ($currency == "JPY") {
            $significance = 1;
            if (is_numeric($number) && is_numeric($significance)){
                $output = ceil($number/$significance)*$significance;
            }
        } else {
            if (is_numeric($number) && is_numeric($significance)){
                $output = $this->dot(ceil($number/$significance)*$significance);
            }
        }

        return $output;
    }

    public function newLine()
    {
        echo "\n";
    }
}