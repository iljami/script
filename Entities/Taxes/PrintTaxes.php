<?php

namespace Entities\Taxes;

require_once './vendor/autoload.php';

use Entities\CashOut\NaturalCashOut;
use Entities\Counter\TaxCounter;
use Entities\Helper\FileMoneyRecordHelper;

class PrintTaxes
{
    public function printFromArray($file_contents_array){

        try {
            if (empty($file_contents_array)){
                throw new \Exception("\n Array containing input file data is empty \n");
            } else {
                $taxCounter = new TaxCounter();
                // currency rate array
                // currency => rate
                $currency_rates = $taxCounter->getCurrencyRates();
                $helper = new FileMoneyRecordHelper();
                $array_of_history = array();

                // indexes of $line
                // 0 - date,
                // 1 - person id,
                // 2 - person type,
                // 3 - operation type,
                // 4 - amount of cash,
                // 5 - currency

                foreach ($file_contents_array as $line) {

                    $date = $line[0];
                    $person_id = $line[1];
                    $person_type = $line[2];
                    $operation_type = $line[3];
                    $amount_of_cash = $line[4];
                    $currency = $line[5];

                    if ($operation_type == 'cash_in') {
                        $output = $taxCounter->cashInTax((float)$amount_of_cash, $currency);
                        echo $helper->roundUp($output, $currency);
                        $helper->newLine();
                    } elseif ($person_type == 'legal' && $operation_type == 'cash_out') {
                        $output = $taxCounter->cashOutTaxLegal((float)$amount_of_cash, $currency);
                        echo $helper->roundUp($output, $currency); //rounds up the value and prints it
                        $helper->newLine();
                    } elseif ($person_type == 'natural' && $operation_type == 'cash_out') {
                        if (!$helper->cashOutRecordExists($person_id, $array_of_history)) {
                            // an array of objects, person_id => object
                            $array_of_history[$person_id] = new NaturalCashOut();
                            $currentNaturalCashOutObject = $array_of_history[$person_id];
                            $current_currency_rate = $currency_rates[$currency];

                            $max_limit = $currentNaturalCashOutObject->getMaxLimit();
                            $cash_not_taken = $currentNaturalCashOutObject->getCashNotTaken();

                            // first operation always has max limit
                            $output = $taxCounter->cashOutTaxNatural(
                                $amount_of_cash, $currency, $cash_not_taken, $max_limit
                            );

                            echo $helper->roundUp($output, $currency);
                            //updating the object and calculating the limit and times when cash was taken out
                            $currentNaturalCashOutObject->update(
                                $date, $amount_of_cash, $currency, $current_currency_rate, $max_limit
                            );

                            $helper->newLine();
                        } elseif ($helper->cashOutRecordExists($person_id, $array_of_history)) {
                            //calculation of times cash was taken and limit
                            $currentNaturalCashOutObject = $array_of_history[$person_id];
                            $current_currency_rate = $currency_rates[$currency];
                            $data = $currentNaturalCashOutObject->calculateTimesAndLimit($date);

                            $times_taken = $data['times_cash_taken'];
                            $limit_left = $data['limit_left'];

                            $output = $taxCounter->cashOutTaxNatural(
                                $amount_of_cash, $currency, $times_taken, $limit_left
                            );

                            echo $helper->roundUp($output, $currency);

                            $currentNaturalCashOutObject->update(
                                $date, $amount_of_cash, $currency, $current_currency_rate, $limit_left
                            );

                            $helper->newLine();
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
