<?php

namespace Entities\Counter;

class TaxCounter
{
    //default currency values
    private $currency_rates = array(
        'EUR' => 1,
        'USD' => 1.1497,
        'JPY' => 129.53
    );
    //set the ratio between euro:currency
    public function setCurrencyRates($currency_rates_array)
    {
        try {
            if (empty($currency_rates_array)) {
                throw new \Exception("\n Array does not contain any currency data. Using default parameters \n");
            } else {
                $this->currency_rates = $currency_rates_array;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCurrencyRates()
    {
        return $this->currency_rates;
    }

    private $cashInSettings = array(
        'tax' => 0.0003,
        'max_tax' => 5.00
    );
    // tax has to be entered in percent and max_tax in euro
    public function setCashInSettings($tax, $max_tax)
    {
        try {
            if (empty($tax) || empty($max_tax) || !is_numeric($tax) || !is_numeric($max_tax)){
                throw new \Exception("\n Invalid Cash in parameters. Using default parameters. \n");
            } else {
                $this->cashInSettings = array(
                    'tax' => $tax / 100,
                    'max_tax' => $max_tax
                );
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    private $cashOutLegalSettings = array(
        'tax' => 0.003,
        'min_tax' => 0.50
    );
    // tax has to be entered in percent and min_tax in euro
    public function setCashOutLegalSettings($tax, $min_tax)
    {
        try {
            if (empty($tax) || empty($min_tax)){
                throw new \Exception("\n Cash out Legal settings parameters are empty. Using default parameters. \n");
            } else {
                $this->cashOutLegalSettings = array(
                    'tax' => $tax / 100,
                    'min_tax' => $min_tax
                );
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    private $cashOutNaturalSettings = array(
        'tax' => 0.003,
        'times_limit' => 3
    );
    // tax has to be entered in percent and times_limit is an int
    public function setCashOutNaturalSettings($tax, $times_limit)
    {
        try {
            if (empty($tax) || empty($times_limit) || !is_int($times_limit) || !is_numeric($tax) || !is_numeric($times_limit)) {
                throw new \Exception("\n Invalid input for Natural Cash Out Operation settings. Using default parameters \n");
            } else {
                $this->cashOutNaturalSettings = array(
                    'tax' => $tax / 100,
                    'times_limit' => $times_limit
                );
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function cashInTax($cash_in, $currency)
    {
        try {
            if ($cash_in <= 0 || $this->currency_rates[$currency] <= 0){
                throw new \Exception("\n Invalid input for Cash In Operation. \n");
            } else {
                $settings = $this->cashInSettings;
                $currency_list = $this->currency_rates;
                $tax = ($cash_in * $currency_list[$currency]) * $settings['tax'];

                if ($tax >= $settings['max_tax'] * $currency_list[$currency]){
                    return $settings['max_tax'] * $currency_list[$currency];
                } else {
                    return $tax;
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function cashOutTaxNatural($cash_out, $currency, $times_cash_taken, $limit_left) //limit left is in EUR
    {
        try {
            if ($cash_out <= 0 || $this->currency_rates[$currency] <= 0 || !is_int($times_cash_taken)){
                throw new \Exception("\n Invalid input for Natural Cash Out Operation. \n");
            } else {
                $settings = $this->cashOutNaturalSettings;
                $currency_list = $this->currency_rates;
                $limit_left *= $currency_list[$currency];

                if ($times_cash_taken < $settings['times_limit'] && $limit_left > 0) {
                    if ($limit_left >= $cash_out) {
                        return 0; // no tax, because free cash out limit is not exceeded
                    } elseif ($limit_left < $cash_out) {
                        $cash_out -= $limit_left; // substract the sum which is free of cost to cash out
                        return $cash_out * $settings['tax']; // apply the tax to the remaining amount
                    }
                } elseif ($times_cash_taken >= $settings['times_limit'] || $limit_left == 0) {
                    return $cash_out * $settings['tax'];
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function cashOutTaxLegal($cash_out, $currency)
    {
        try {
            if ($cash_out <= 0 || $this->currency_rates[$currency] <= 0){
                throw new \Exception("\n Invalid input for Legal Cash Out Operation. \n");
            } else {
                $settings = $this->cashOutLegalSettings;
                $currency_list = $this->currency_rates;
                $tax = ($cash_out * $currency_list[$currency]) * $settings['tax'];

                if ($tax <= $settings['min_tax'] * $currency_list[$currency]){
                    return $settings['min_tax'] * $currency_list[$currency];
                } else {
                    return $tax;
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
