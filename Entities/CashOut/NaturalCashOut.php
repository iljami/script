<?php

namespace Entities\CashOut;

class NaturalCashOut
{
    public $cash_out_history;
    public $limit_left = 0;
    public $times_cash_taken = 0;
    public $current_week;
    public $current_year;
    // weekly cash limit in euro
    public $max_limit = 1000;
    // cash was not taken this week
    // value stands for the times the cash was taken
    public $cash_not_taken = 0;
    // all weekly free cashouts were used
    public $no_limit_left = 0;

    public function getMaxLimit()
    {
        return $this->max_limit;
    }

    public function getCashNotTaken()
    {
        return $this->cash_not_taken;
    }

    public function updateCashOutHistory($date, $cash, $currency)
    {
        try {
            if (empty($date) || empty($cash) || empty($currency)){
                throw new \Exception("\n Parameters of Update Cash Out History cannot be empty! \n");
            } else {
                $cash_out = array($date, $cash, $currency);
                $this->cash_out_history[] = $cash_out;
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    public function getCashOutHistory()
    {
        return $this->cash_out_history;
    }

    public function getLastCashOutRecord()
    {
        $length = sizeof($this->cash_out_history);
        if ($length == 0){
            return false;
        } else {
            return $this->cash_out_history[$length - 1];
        }
    }

    public function setLimitLeft($limit_left)
    {
        $this->limit_left = $limit_left;
    }

    public function getLimitLeft()
    {
        return $this->limit_left;
    }

    public function calculateLimitLeft($cash_taken, $limit_left, $currency_rate)
    {
        try {
            if ($cash_taken <= 0 || !isset($limit_left) || $currency_rate <= 0){
                throw new \Exception("\n Calculate Limit Left - Invalid parameters \n");
            } else {
                $current_limit = $limit_left * $currency_rate;

                if ($current_limit > $cash_taken){
                    return ($current_limit - $cash_taken) / $currency_rate;
                } else if ($current_limit <= $cash_taken) {
                    return $this->no_limit_left;
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateTimesCashTaken()
    {
        $this->times_cash_taken += 1;
    }

    public function setTimesCashTaken($times_cash_taken)
    {
        $this->times_cash_taken = $times_cash_taken;
    }

    public function getTimesCashTaken()
    {
        return $this->times_cash_taken;
    }
    // returns the number of the current week in the year given
    public function weekNumberFromDate($date)
    {
        try {
            if (!isset($date)){
                throw new \Exception('Week number from date - parameters cannot be empty');
            } else {
                $new_date = new \DateTime($date);
                return $new_date->format("W");
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }

    }

    public function yearFromDate($date)
    {
        try {
            if (!isset($date)){
                throw new \Exception('Year number from date - parameters cannot be empty');
            } else {
                $new_date = new \DateTime($date);
                return $new_date->format("Y");
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    public function updateCurrentWeek()
    {
        $record = $this->getLastCashOutRecord();
        try {
            if (empty($record)){
                throw new \Exception("There are no records of cash out operation");
            } else {
                $event = $record[0]; // the date of actions

                $this->current_week = $this->weekNumberFromDate($event);
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    public function getCurrentWeek()
    {
        $current_week = $this->current_week;
        if (empty($current_week)){
            return false;
        } else {
            return $this->current_week;
        }
    }

    public function updateCurrentYear()
    {
        $record = $this->getLastCashOutRecord();
        try {
            if (empty($record)){
                throw new \Exception("There are no records of cash out operation");
            } else {
                $event = $record[0]; // the date of actions

                $this->current_year = $this->yearFromDate($event);
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }
    }

    public function getCurrentYear()
    {
        $current_year = $this->current_year;
        if (empty($current_year)){
            return false;
        } else {
            return $this->current_year;
        }
    }

    public function update($date, $cash, $currency, $currency_rate, $limit_left)
    {
        try {
            if (empty($date) || empty($currency) || empty($currency_rate) || !isset($cash) || !isset($limit_left)){
                throw new \Exception("\n Invalid parameters for natural cash out object update \n");
            } else {
                $new_limit = 0;
                $old_limit = $limit_left;

                $week = $this->getCurrentWeek();
                $year = $this->getCurrentYear();

                $new_year = $this->yearFromDate($date);
                $new_week = $this->weekNumberFromDate($date);

                // reduce times cash was taken because
                // it's a new week and the limit restarts
                if ($new_year == $year) {
                    if ($new_week != $week) {
                        $this->setTimesCashTaken($this->cash_not_taken);
                    }
                } elseif ($new_year != $year && $new_year == ($year + 1)) {
                    if ($new_week != $week) {
                        $this->setTimesCashTaken($this->cash_not_taken);
                    }
                } elseif ($new_year != $year && $new_year != ($year + 1)) {
                    $this->setTimesCashTaken($this->cash_not_taken);
                }

                $this->updateCashOutHistory($date, $cash, $currency);
                $this->updateCurrentWeek();
                $this->updateCurrentYear();
                $this->updateTimesCashTaken();

                $new_limit = $this->calculateLimitLeft($cash, $old_limit, $currency_rate);

                $this->setLimitLeft($new_limit);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function calculateTimesAndLimit($new_action_date)
    {
        try {
            if(empty($new_action_date)){
                throw new \Exception("\n No date for Times And Limit Calculation specified \n");
            } else {
                $data = array();

                $week = $this->getCurrentWeek();
                $year = $this->getCurrentYear();

                $new_year = $this->yearFromDate($new_action_date);
                $new_week = $this->weekNumberFromDate($new_action_date);

                if ($new_year == $year) {
                    if ($new_week == $week) {
                        $data['limit_left'] = $this->limit_left;
                        $data['times_cash_taken'] = $this->times_cash_taken;
                    } elseif ($new_week != $week) {
                        $data['limit_left'] = $this->max_limit;
                        $data['times_cash_taken'] = $this->cash_not_taken;
                    }
                } elseif ($new_year != $year && $new_year == ($year + 1)) {
                    if ($new_week == $week) {
                        $data['limit_left'] = $this->limit_left;
                        $data['times_cash_taken'] = $this->times_cash_taken;
                    } elseif ($new_week != $week) {
                        $data['limit_left'] = $this->max_limit;
                        $data['times_cash_taken'] = $this->cash_not_taken;
                    }
                } elseif ($new_year != $year && $new_year != ($year + 1)) {
                    $data['limit_left'] = $this->max_limit;
                    $data['times_cash_taken'] = $this->cash_not_taken;
                }

                return $data;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
