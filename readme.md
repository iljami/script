This is a script for calculation operations.

Launching application:

    1) Open the command line.

    2) Change directory to the project root.

    3) Enter php script.php YourFileNameHere

    4) Change YourFileNameHere to your file name.

    5) If no file is present, the test.csv file in the project root can be used
        to test the script.

To launch the application tests:

    1) Change directory to the project root.

    2) Launch the file located in vendor/bin/phpunit (in UNIX systems the tests can be started by entering
        the following command to the terminal ./vendor/bin/phpunit)
